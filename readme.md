readme.md - README for GambasAppUtilityLib v0.4


Purpose:
Library for controlling status bar and other common GUI features in Qt or Gtk based Gambas UI.

Requires GambasAppUtilityLib library 0.4 or later.

Note: Gambas3 projects ('.project') are looking for libraries ('<filename>.gambas') in /home/<user_name>/.local/share/gambas3/lib/<vendor_prefix>/.Make (Project|Make|Executable...) writes the executable there in addition to the root of the project's directory.


Usage notes:

~...


Setup:

~...


0.4: 
~add license file
~add readme
0.3: 
~internationalization prep
0.2: 
~add StaleIcon similar to DirtyIcon
0.1: 
~initial release with status bar logic.

Fixes:

Known Issues:
~

Possible Enhancements:


Steve Sepan
ssepanus@yahoo.com
2/14/2022